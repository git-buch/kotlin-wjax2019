# Kotlin Workshop - W-JAX 2019

von René Preißel und Bjørn Stachmann

## Workshop - Konzept

* Kotlin für Java-Entwickler praxisnah vorstellen 
* "Hands on"  
    * Schneller Einstieg in praktische Übungen
    * Durchgängiges Beispiel auf Basis einer Webanwendung
    * Bei Null starten und jede Codezeile selber schreiben 
    * Viel Raum zum experimentieren
* Nur die wichtigsten Features vorstellen
* Fokus auf Unterschiede zu Java
* Am Ende ein Ausblick auf weitere coole Features von Kotlin

## Agenda

### Intro und Vorstellung

### Phase I: *Mehr als nur "Hallo Welt"*

Das Gerüst für eine Webanwendung wird aufgebaut. 
Sie erlernen dabei: Grundlegende Syntax, 
einfache Funktionen deklarieren und aufrufen, 
einfache Logiken und Fallunterscheidungen, 
das Starten eines Web-Servers 
und das Bearbeiten von http-Requests mit Hilfe von Ktor.

[Einstieg in Kotlin und Ktor (Lektion 1)](teil1-kotlin-und-ktor/index.md), [Übung 1](uebungen/Uebung1.md)\
[Funktionen und Datentypen (Lektion 2)](teil2-funktionen-und-datentypen/index.md), [Übung 2](uebungen/Uebung2.md)

### Phase II: *Hübscher ist besser!*

Mit Hilfe der HTML-DSL (Doman Specific Language) wird die Anwendung "aufgehübscht". 
Dabei lernen Sie, wie DSLs in Kotlin funktionieren, 
wie man sie verwendet, 
und auch ganz einfach erweitern kann. 
Nebenher erfahren Sie einiges über das Typsystem von Kotlin.

[Klassen, Extensions und DSLs (Lektion 3)](teil3-klassen-extensions-und-dsls/index.md), [Übung 3](uebungen/Uebung3.md)

### Phase III: *Das Herz der Anwendung*

Jetzt wird die Anwendungslogik ausgebaut. 
Kotlin- und auch Java-Bibliotheken werden aufgerufen, 
eigene Klassen erstellt. 
Wir zeigen: Collections, Fallunterscheidungen, Klassen und Objekte, 
Konstruktoren und ein paar nützliche Tricks.

[Klassen, Maps (Lektion 4)](teil4-klassen-und-maps/index.md), [Übung 4](uebungen/Uebung4.md)

### Phase IV: *Und der ganze Rest der Welt …*

Wenn mehrere Anwender parallel bedient werden müssen 
und Austausch mit anderen Servern erforderlich wird, 
fängt es an, wirklich interessant zu werden. 
Wir zeigen, was Kotlin zu bieten hat: 
Asynchrone I/O, Koroutinen, Ktor-Session-Handling, Web-Sockets etc.

[REST Services und Data Classes (Lektion 5)](teil5-rest-services-und-data-classes/index.md), [Übung 5](uebungen/Uebung5.md)\
[Testen (Lektion 6)](teil6-testen/index.md)\
[Ausblick (Lektion 7)](teil7-ausblick/index.md)


### Fragen

## Zeiten
 
 | Wann                 | Was                                           |
 |----------------------|-----------------------------------------------|
 |  09:00 - 09:15       | **Intro**, Vorstellung                        |
 |   **Phase I**        | **Mehr als nur "Hallo Welt"**                 |
 |  09:15 - 09:30       | Einstieg in Kotlin und Ktor (Lektion 1)       |
 |  09:30 - 10:00       | Übung 1                                       |
 |  10:00 - 10:30       | Funktionen und Datentypen (Lektion 2)         |
 |    **Pause**         |                                               |
 |  11:00 - 11:30       | Übung 2                                       |
 |   **Phase II**       | **Hübscher ist besser!**                      |
 |  11:30 - 12:00       | Klassen, Extensions und DSLs (Lektion 3)      |
 |  12:00 - 12:30       | Übung 3                                       |
 |   **Mittag**         |                                               |
 |  **Phase III**       | **Das Herz der Anwendung!**                   |
 |  13:30 - 13:45       | Klassen, Maps (Lektion 4)                     |
 |  13:45 - 14:15       | Übung 4                                       |
 |  **Phase VI**        | **Und der ganze Rest der Welt ...**           |
 |  14:15 - 14:30       | REST Services und Data Classes (Lektion 5)    |
 |  14:30 - 15:00       | Übung 5                                       |
 |      Pause           |                                               |
 |  15:30 - 15:45       | KoTest und Mockk (Lektion 6)                  |
 |  15:45 - 16:15       | Übung 6                                       |
 |  16:15 - 17:00       | Ausblick und Fragen (Lektion 7)               |


## Kontakt

 | Name                 | Twitter       | Email                         |
 |----------------------|---------------|-------------------------------|
 | **René Preißel**     | @RenePreissel | rene.preissel@etosquare.de    |
 | **Bjørn Stachmann**  | @old_stachi   | bjoernarno.stachmann@otto.de  |

# Setup und Installation

## Software Voraussetzungen
 
* [Java 8 / 11 / 13 ](http://jdk.java.net/13/)
* [IntellijIDEA Community Edition 2019.2](https://www.jetbrains.com/idea/download/)


## Beispiele und Übungen auf den eigenen Rechner kopieren

Software für Plattform vom USB-Stick kopieren oder 
von Gitlab https://gitlab.com/git-buch/kotlin-wjax2019.git klonen


## Installation und Einrichtung von Intellij

1. Für Windows: Installieren Sie 7-Zip-Portable
    * Nutzen Sie 7-Zip für die nachfolgenden Entpack-Schritte
1. Wenn kein passendes JDK (8 / 11 /13 ) vorhanden ist, entpacken Sie das OpenJdk-Archive, 
1. Entpacken Sie das gradle_home.zip 
1. Installieren Sie die Intellij IDEA Community Edition
1. Starten Sie Intellij IDEA und öffnen Sie die Preferences\
  ![](doc/installation/01_PreferencesOeffnen.png)
1. Setzen Sie das Gradle-Home auf das entpackte Verzeichnis\
  ![](doc/installation/02_GradleHome_anpassen.png)
1. Importieren Sie das Gradle Projekt\
  ![](doc/installation/03_Projekt_Importieren.png)


