package de.e2.testen

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestContext
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication

fun TestContext.jsonRequest(
        module: Application.() -> Unit,
        method: HttpMethod,
        uri: String,
        body: String,
        check: TestContext.(response: String) -> Unit
) {
    withTestApplication(module) {
        val call = handleRequest(method, uri) {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(body)
        }
        with(call) {
            response.status() shouldBe HttpStatusCode.OK
            val contentType = response.headers[HttpHeaders.ContentType]
            contentType.shouldNotBeNull()
            ContentType.parse(contentType).withoutParameters() shouldBe ContentType.Application.Json
            val content = response.content
            content.shouldNotBeNull()
            check(content)
        }
    }
}

class AdderKtorTest : StringSpec({

    "addRequest" {
        withTestApplication(Application::adderApi) {
            val call = handleRequest(HttpMethod.Post, "/add") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(
                        """ 
                    {
                      "arg1": 10,
                      "arg2": 20
                    }
                    """
                )
            }
            with(call) {
                val contentType = response.headers[HttpHeaders.ContentType]
                val content = response.content

                response.status() shouldBe HttpStatusCode.OK

                contentType.shouldNotBeNull()
                ContentType.parse(contentType).withoutParameters() shouldBe ContentType.Application.Json

                content.shouldNotBeNull()
                jacksonObjectMapper().readValue<AddResult>(content) shouldBe AddResult(30)
            }
        }
    }



    "addRequest compact" {
        jsonRequest(
                Application::adderApi,
                HttpMethod.Post,
                "/add",
                """ 
                    {
                      "arg1": 10,
                      "arg2": 20
                    }
                    """
        ) { response ->
            jacksonObjectMapper().readValue<AddResult>(response) shouldBe AddResult(30)
        }
    }

})