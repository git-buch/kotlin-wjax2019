package de.e2.testen

import io.kotest.assertions.assertSoftly
import io.kotest.inspectors.forAtLeastOne
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.inspectors.forAll
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.contain
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.beLowerCase
import io.kotest.matchers.string.shouldBeUpperCase
import io.kotest.matchers.string.shouldContain
import io.kotest.property.Exhaustive
import io.kotest.property.checkAll
import io.kotest.property.exhaustive.ints


class KoTest : StringSpec({

    "length should return size of string" {
        "hello".length shouldBe 5
    }

    "chars should be lowercase" {
        "word" should beLowerCase()
    }

    "chars should be UPPERCASE" {
        "WORD".shouldBeUpperCase()
    }

    "sets" {
        setOf("Stoustrup", "Jeremow", "Gosling") should contain("Jeremow")
    }

    "collection inspectors" {
        val list = listOf("January", "February", "March")
        list.forAtLeastOne {
            it shouldContain "y"
        }

        list.forAll {
            it[0].isUpperCase().shouldBeTrue()
        }
    }

    fun fib(n: Int): Int =
        when {
            n < 1 -> 1
            n == 1 -> 1
            n == 2 -> 1
            else -> fib(n - 1) + fib(n - 2)
        }

    "fib data driven test" {
        assertSoftly { // run all asserts and report all failures
            forAll(
                row(1, 1),
                row(2, 1),
                row(3, 2),
                row(4, 3),
                row(5, 5),
                row(6, 8)
            ) { n, f -> fib(n) shouldBe f }
        }
    }

    fun fib2(n: Int): Int {
        var a = 0
        var b = 1
        for (i in 1 until n) {
            val sum = a + b
            a = b
            b = sum
        }
        return b
    }

    "fib property based test" {
        checkAll(Exhaustive.ints(-20..20)) { n ->
            fib2(n) shouldBe fib(n)
        }
    }
})