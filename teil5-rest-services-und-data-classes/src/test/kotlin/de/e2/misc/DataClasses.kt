package de.e2.misc

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class DataClasses {

    @Test
    fun `reguläre Klasse`() {
        class Rechteck(val x: Int, val y: Int)

        val r1 = Rechteck(3,4)

        assertTrue(r1.toString().startsWith("de.e2.misc.DataClasses\$reguläre Klasse\$Rechteck@"))

        val r2 = Rechteck(3,4)

        assertNotEquals(
                r1,
                r2,
                "Weil die Klasse Rechteck keine passende `equals()`-Methode implementiert"
        )
    }

    @Test
    fun `data class`() {

        data class Rechteck(val x: Int, val y: Int)

        val r1 = Rechteck(3,4)

        assertEquals("Rechteck(x=3, y=4)", r1.toString())

        val r2 = Rechteck(3,4)

        assertEquals(
                r1,
                r2,
                "Weil Data-Classes attributbezogene `equals()`-Methode implementieren"
        )
    }

    @Test
    fun `data classes, weiter Features`() {

        data class Rechteck(val x: Int, val y: Int)

        val r1 = Rechteck(3,4)

        val r2 = r1.copy(y=11)

        assertEquals(3, r2.x)
        assertEquals(11, r2.y)
    }

    @Test
    fun `data classes und destructuring`() {

        data class Rechteck(val x: Int, val y: Int)

        val r1 = Rechteck(3,4)

        val (a, b) = r1  // Destructuring, Aufbrechen in Einzelteile

        assertEquals(3, a)
        assertEquals(4, b)

        listOf(Rechteck(3,4), Rechteck(3,3))
                .forEach { (dasX,dasY) ->
                    println("Fläche: ${dasX * dasY}")
                }


    }

}