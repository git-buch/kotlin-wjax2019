package de.e2.ktor.p01_helloworld.a

import io.ktor.application.call
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty


fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/hello5") {
                call.respondTextWriter {
                    repeat(5) { index ->
                        write("World $index\n")
                    }

                    //<editor-fold desc="->Java Variante" defaultstate="collapsed">
                    /*

                    repeat(5, index -> {
                        System.out.println("World " + index);
                    });

                    */
                    //</editor-fold>
                }
            }
        }
    }
    println("Open with: http://localhost:8080/hello5")
    server.start(wait = true)
}

//<editor-fold desc="->Repeat Funktion" defaultstate="collapsed">
fun repeat(times: Int, action: (Int) -> Unit): Unit {
    for (index in 1..times) {
        action(index - 1)
    }
}
//</editor-fold>

//<editor-fold desc="->Java Variante" defaultstate="collapsed">
/*
public static void repeat(int times, Consumer<Integer> action) {
    for (int i = 1; i <= times; i++) {
        action.accept(i - 1);
    }
}
*/
//</editor-fold>
