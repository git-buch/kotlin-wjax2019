# Einstieg in Kotlin und Ktor

## Kotlin Entstehung

* Kotlin ist auch eine Insel - vor Sankt Petersburg 
* JetBrains stellt die Hauptentwickler der Sprache 
* __2011 Erste veröffentlichte Version__ 
* 2012 Unter Apache-2-Lizenz gestellt
* 2012 KotlinJS wird vorgestellt
* __2016 Version 1.0 veröffentlicht__
* 2017 Kotlin Native wird vorgestellt
* __2017 Kotlin wird offizielle Sprache für Android__
* __2019 Kotlin wird erste Sprache für Android__


## Umfragen

* [Google Trend 3 Jahre](https://trends.google.de/trends/explore?date=2016-08-01%202019-10-28&q=%2Fm%2F0_lcrx4,%2Fm%2F091hdj)
* [Stack Overlflow Umfrage 2019](https://insights.stackoverflow.com/survey/2019#most-loved-dreaded-and-wanted)
* [Umfrage März 2018](https://pusher.com/state-of-kotlin?section=dev-favorite)


## Design Prinzipien Kotlin

* Interoparabilität mit Java 
* Einfachheit und Lesbarkeit
* Starke Typisierung
* Gute Unterstützung von DSLs
* Schneller Compiler 
* Pragramatische Lösungen vor akademischer Vollständigkeit
* Unterstützung von objektorientierter, prozeduraler und funktionaler Programmierung

## Ktor - Webframework

* "framework for building asynchronous servers and clients"
* In Kotlin geschrieben und nutzt viele Fähigkeiten der Sprache


## Und so sieht das dann aus:

* [Einstieg Kotlin und Ktor (rp)](src/main/kotlin/de/e2/helloworld/HelloWorld_1.kt)
* [Lambdas (rp)](src/main/kotlin/de/e2/helloworld/HelloWorld_2a_Repeat.kt) 
            ([B](src/main/kotlin/de/e2/helloworld/HelloWorld_2b_Repeat.kt))
* [Uebung 1](../uebungen/Uebung1.md)            
