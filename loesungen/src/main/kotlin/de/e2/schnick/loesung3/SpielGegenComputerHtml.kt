package de.e2.schnick.loesung3

import de.e2.schnick.loesung3.Wahl.PAPIER
import de.e2.schnick.loesung3.Wahl.STEIN
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.BODY
import kotlinx.html.ButtonType
import kotlinx.html.FormMethod
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.form
import kotlinx.html.h1
import kotlinx.html.label
import kotlinx.html.p

enum class Wahl {
    STEIN,
    PAPIER
}

fun String.toWahl(): Wahl? =
    when (toLowerCase()) {
        "stein" -> STEIN
        "papier" -> PAPIER
        else -> null
    }

fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl): String {
    if (spielerWahl == computerWahl) {
        return "Unentschieden"
    }

    if (spielerWahl == PAPIER) {
        return "Spieler hat gewonnen"
    }

    return "Computer hat gewonnen"
}

val auswahl = listOf(STEIN, PAPIER)

fun Application.textModul() {
    routing {
        get("/spiel") {
            val wahl = call.parameters["wahl"]
            if (wahl == null) {
                call.respondText("Bitte Parameter 'wahl' angeben!")
            } else {
                val spielerWahl = wahl.toWahl()
                if (spielerWahl == null) {
                    call.respondText("Ungültige Auswahl!")
                } else {
                    val computerWahl = auswahl.random()
                    val ergebnis = ermittleErgebnis(spielerWahl, computerWahl)

                    call.respondText("Spieler hat gewählt $wahl.\nComputer hat gewählt $computerWahl.\nDas Ergebnis ist: $ergebnis.")
                }
            }
        }
    }
}

fun Application.htmlModul() {
    routing {
        get("/spielhtml") {
            call.respondHtml {
                body {
                    h1 {
                        text("Schnick Schnack Schnuck")
                    }
                    form(action = "/spielergebnis", method = FormMethod.get) {
                        label {
                            text("Deine Wahl:")
                        }
                        button(type = ButtonType.submit, name = "wahl") {
                            value = "papier"
                            text("Papier")
                        }
                        button(type = ButtonType.submit, name = "wahl") {
                            value = "stein"
                            text("Stein")
                        }
                    }
                }
            }
        }

        get("/spielergebnis") {
            val wahl = call.parameters["wahl"]
            if (wahl == null) {
                call.respondText("Bitte Parameter 'wahl' angeben!")
            } else {
                val computerWahl = auswahl.random()
                val spielerWahl = wahl.toWahl()
                if (spielerWahl == null) {
                    call.respondText("Ungültige Auswahl!")
                } else {
                    val ergebnis = ermittleErgebnis(spielerWahl, computerWahl)

                    call.respondHtml {
                        body {
                            h1 {
                                text("Schnick Schnack Schnuck")
                            }
                            paragraph("Spieler hat gewählt: $wahl")
                            paragraph("Computer hat gewählt: $computerWahl")
                            paragraph("Das Ergebnis ist: $ergebnis")

                            div {
                                a(href = "/spielhtml") {
                                    text("Nächstes Spiel")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        htmlModul()
        textModul()
    }

    server.start(wait = true)
}

fun BODY.paragraph(s: String) =
    p {
        text(s)
    }
