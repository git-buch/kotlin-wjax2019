package de.e2.schnick.loesung5.ext

import de.e2.schnick.loesung5.ext.Ergebnis.COMPUTER_GEWINNT
import de.e2.schnick.loesung5.ext.Ergebnis.SPIELER_GEWINNT
import de.e2.schnick.loesung5.ext.Ergebnis.UNENTSCHIEDEN
import de.e2.schnick.loesung5.ext.Wahl.PAPIER
import de.e2.schnick.loesung5.ext.Wahl.SCHERE
import de.e2.schnick.loesung5.ext.Wahl.STEIN
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

enum class Ergebnis {
    SPIELER_GEWINNT,
    COMPUTER_GEWINNT,
    UNENTSCHIEDEN
}

enum class Wahl {
    STEIN, PAPIER, SCHERE
}


fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl) = when {
    spielerWahl == computerWahl -> UNENTSCHIEDEN
    spielerWahl == PAPIER && computerWahl == STEIN -> SPIELER_GEWINNT
    spielerWahl == STEIN && computerWahl == SCHERE -> SPIELER_GEWINNT
    spielerWahl == SCHERE && computerWahl == PAPIER -> SPIELER_GEWINNT
    else -> COMPUTER_GEWINNT
}


fun string2Wahl(wahl: String): Wahl? =
    when (wahl.toLowerCase()) {
        "stein" -> STEIN
        "papier" -> PAPIER
        "schere" -> SCHERE
        else -> null
    }


val auswahl = Wahl.values()

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/spiel") {
                val wahl = call.parameters["wahl"]
                if (wahl == null) {
                    call.respondText("Bitte Parameter 'wahl' angeben!")
                } else {
                    val spielerWahl = string2Wahl(wahl)
                    if (spielerWahl == null) {
                        call.respondText("Ungültige Wahl!")
                    } else {
                        val computerWahl = auswahl.random()
                        val ergebnis = ermittleErgebnis(spielerWahl, computerWahl)

                        call.respondText("Spieler hat gewählt $wahl.\nComputer hat gewählt $computerWahl.\nDas Ergebnis ist: $ergebnis.")
                    }
                }

            }
        }
    }

    server.start(wait = true)
}
