package de.e2.schnick.loesung5

import de.e2.schnick.loesung5.Wahl.PAPIER
import de.e2.schnick.loesung5.Wahl.STEIN
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty



enum class Wahl {
    STEIN,
    PAPIER
}

fun string2Wahl(wahl: String): Wahl? =
    when (wahl.toLowerCase()) {
        "stein" -> STEIN
        "papier" -> PAPIER
        else -> null
    }

fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl): String {
    if (spielerWahl == computerWahl) {
        return "Unentschieden"
    }

    if (spielerWahl == PAPIER) {
        return "Spieler hat gewonnen"
    }

    return "Computer hat gewonnen"
}


val auswahl = listOf(STEIN, PAPIER)

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/spiel") {
                val wahl = call.parameters["wahl"]
                if (wahl == null) {
                    call.respondText("Bitte Parameter 'wahl' angeben!")
                } else {
                    val spielerWahl = string2Wahl(wahl)
                    if (spielerWahl == null) {
                        call.respondText("Ungültige Auswahl!")
                    } else {
                        val computerWahl = auswahl.random()
                        val ergebnis = ermittleErgebnis(spielerWahl, computerWahl)

                        call.respondText("Spieler hat gewählt $wahl.\nComputer hat gewählt $computerWahl.\nDas Ergebnis ist: $ergebnis.")
                    }
                }

            }
        }
    }

    server.start(wait = true)
}
