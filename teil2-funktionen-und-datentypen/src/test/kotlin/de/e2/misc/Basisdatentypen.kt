package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.ClassCastException
import kotlin.math.roundToInt
import kotlin.math.tan

class Basisdatentypen {

    /**
     * Anders als Java, unterscheidet Kotlin nicht zwischen
     * primitiven Datentypen und Objekten.
     * Auch Integer, Doubles etc. sind Objekte in Kotlin
     * und gehören regulären Klassen an.
     */
    @Test
    fun `Auch Zahlen sind Objekte und haben Methoden`() {

        val dieZahl = 42

        assertEquals("42", dieZahl.toString())
        assertEquals(42.0, dieZahl.toDouble())
        assertEquals(-42, dieZahl.unaryMinus())
        assertEquals(-42, -dieZahl)
    }

    /**
     * Man beachte die Großbuchstaben bei den Basisdatentypen,
     * z. B. `Long` statt `long`.
     */
    @Test
    fun `Ein paar Datentypen, die in Java primitiv sind`() {

        val zahlInt: Int = 42
        val zahlLong: Long = 42L
        val zahlFloat: Float = 42.0F
        val zahlDouble: Double = 42.0
        val boolean: Boolean = true
        val character: Char = 'B'

        assertEquals("kotlin.Int", zahlInt::class.qualifiedName)
        assertEquals("kotlin.Long", zahlLong::class.qualifiedName)
        assertEquals("kotlin.Float", zahlFloat::class.qualifiedName)
        assertEquals("kotlin.Double", zahlDouble::class.qualifiedName)
        assertEquals("kotlin.Boolean", boolean::class.qualifiedName)
        assertEquals("kotlin.Char", character::class.qualifiedName)
    }

    @Test
    fun `Rechnen kann man natürlich auch`() {
        assertEquals(42, 5 * 8 + 2)
        assertEquals(6.0093273882816955, tan(0.5) * 11)
    }

    @Test
    fun `Für das Umwandeln zwischen Basidatentypen gibt es Methoden`() {
        val zahl = 42.7
        assertEquals(42, zahl.toInt())
        assertEquals(43, zahl.roundToInt())

        // Es gibt auch einen Cast-Operator `as`.
        assertEquals(42.7,zahl as Number)

        // Anders als in Java werden beim Cast keine Datenumwandlungen durchgeführt.
        // Hier gibt es eine Exception, weil ein `Double` eben kein `Int` ist.
        assertThrows<ClassCastException> { println(zahl as Int) }
    }

    @Test
    fun `Na und Strings gibt es natürlich auch`() {

        val einString = "Zweiundvierzig"

        assertEquals("ZWEIUNDVIERZIG", einString.toUpperCase())
        assertEquals(14, einString.length)
        assertEquals('Z', einString[0])
        assertEquals('g', einString.last())
    }


    fun sayHello() : Unit  {
        println("Hello")
    }

    fun sayBye(){
        println("Bye")
    }

    /**
     * In Kotlin haben alle Funktionen und Methoden einen Rückgabewert.
     * `void`-Methoden, die nichts zurückgeben, gibt es nicht.
     * Statt dessen gibt man `Unit` zurück:
     * Eine Klasse von der es nur ein Objekt, Namens `Unit`, gibt.
     */
    @Test
    fun `Der besondere Typ Unit`() {
        val helloResult = sayHello()

        assertEquals(Unit, helloResult)

        assertEquals("kotlin.Unit", ::sayHello.returnType.toString())
        assertEquals("kotlin.Unit",::sayBye.returnType.toString())
    }
}


