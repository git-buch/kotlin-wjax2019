package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Listen {

    /**
     * Kotlin unterscheidet zwischen unveränderbaren und veränderbaren Listen
     * Intern wird immer eine ArrayList benutzt
     */
    @Test
    fun `Listen`() {

        val liste = listOf(23, 42, 4711)

        assertEquals(3, liste.size)

        for (i in liste)
            println("i = $i")

        assertEquals(23, liste[0])
        assertEquals(42, liste[1])
        assertEquals(4711, liste.last())

        val mutableListe = mutableListOf(23, 42, 4711)
        mutableListe[0] = 1
        assertEquals(1, mutableListe.first())
    }

    @Test
    fun `Sets`() {

        val set = setOf(2, 5, 5, 2, 1)

        assertEquals(3, set.size)
        assertTrue(set.contains(5))
    }

    @Test
    fun forEach() {
        val liste = listOf(1, 2, 3)

        liste.forEach { println("Element: $it") }
    }

    @Test
    fun `Map und Filter Operationen`() {

        val liste = listOf(1, 2, 3, 4, 5, 6, 7)

        val squaredListe =
                liste
                        .filter { it <=4 }
                        .map { it * it }

        assertEquals(listOf(1, 4, 9, 16), squaredListe)
    }

}


