package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.DayOfWeek
import java.time.LocalDateTime

class EnumsUndWhen {

    /**
     * Enums werden mit enum class definiert, sind aber sonst gleich wie in Java
     */
    enum class Richtung {
        LINKS, RECHTS
    }

    @Test
    fun `whens sind ein besseres switch ohne break`() {
        val richtung = Richtung.RECHTS
        when (richtung) {
            Richtung.RECHTS -> println("Rechts")
            Richtung.LINKS -> println("Links")
        }
    }

    @Test
    fun `when als Expression`() {
        val richtung = Richtung.RECHTS

        val richtungAsString =
                when (richtung) {
                    Richtung.RECHTS -> "Rechts"
                    Richtung.LINKS -> "Links"
                }

        assertEquals("Rechts", richtungAsString)
    }

    @Test
    fun `when mit else`() {
        val richtungAlsString = "Links"

        val richtungFromString = when (richtungAlsString) {
            "Rechts" -> Richtung.RECHTS
            "Links" -> Richtung.LINKS
            else -> null
        }

        assertEquals(Richtung.LINKS, richtungFromString)

        fun fib(n: Int): Int =
                when (n) {
                    1 -> 1
                    2 -> 1
                    else -> fib(n - 1) + fib(n - 2)
                }

        assertEquals(13, fib(7))
    }

    @Test
    fun `whens mit freien Bedingungen`() {

        val today = LocalDateTime.now()
        when {
            today.hour == 17 ->
                println("Tea Time")

            today.hour == 11 && today.dayOfWeek == DayOfWeek.SUNDAY ->
                println("Brunch")

            else ->
                println("Weiss nicht recht")
        }
    }

}