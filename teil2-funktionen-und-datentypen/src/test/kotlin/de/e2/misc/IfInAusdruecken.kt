package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.DayOfWeek
import java.time.LocalDateTime

class IfInAusdruecken {


    @Test
    fun `Das if-Statement darf auch in Ausdrücken genutzt werden`() {

        val meldung = if( Math.random() > 0.2 ) "Alles gut" else "Nicht OK"

        println("Meldung: $meldung")
    }
}


