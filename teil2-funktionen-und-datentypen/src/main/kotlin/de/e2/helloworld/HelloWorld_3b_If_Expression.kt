package de.e2.ktor.p01_helloworld

import io.ktor.application.call
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/hello/{count}") {
                val countAsStringNullable: String? = call.parameters["count"]

                val countAsInt =
                    if (countAsStringNullable != null) {
                        countAsStringNullable.toInt()
                    } else {
                        5
                    }

                //Geht auch einzeilig
                //val countAsInt: Int = if (countAsString != null) countAsString.toInt() else 5

                call.respondTextWriter {
                    repeat(countAsInt) {
                        write("World $it\n")
                    }
                }
            }
        }
    }
    server.start(wait = true)
}