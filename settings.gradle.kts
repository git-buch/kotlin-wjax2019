rootProject.name = "kotlin-wjax2019"

include(":teil1-kotlin-und-ktor")
include(":teil2-funktionen-und-datentypen")
include(":teil3-klassen-extensions-und-dsls")
include(":teil4-klassen-und-maps")
include(":teil5-rest-services-und-data-classes")
include(":teil6-testen")
include(":teil7-ausblick")
include(":uebungen")
include(":loesungen")
//include(":mpp-chat")

pluginManagement {
    plugins {
        val kotlinVersion = "1.3.72"

        id("org.jetbrains.kotlin.jvm") version kotlinVersion
        id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion
    }
}

