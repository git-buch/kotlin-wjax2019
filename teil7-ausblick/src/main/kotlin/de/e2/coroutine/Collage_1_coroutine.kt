package de.e2.coroutine

import com.jayway.jsonpath.JsonPath
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.response.respondOutputStream
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.awt.image.BufferedImage
import java.io.IOException
import java.io.InputStream
import javax.imageio.ImageIO

fun Application.collage() {
    val ktorClient = HttpClient()
    routing {
        get("/{query}") {
            val query = call.parameters.required("query")
            val count = call.parameters["count"]?.toInt() ?: 20

            val urls: List<String> = ktorClient.requestImageUrls(query, count)
            val images: List<BufferedImage> = urls.map { ktorClient.requestImageData(it) }
            val image = combineImages(images)

            call.respondOutputStream(ContentType.Image.PNG) {
                ImageIO.write(image, "png", this)
            }
        }
    }
}

//<editor-fold desc="Request-Funktionen" defaultstate="collapsed">
suspend fun HttpClient.requestImageUrls(query: String, count: Int = 20): List<String> {
    val json = get<String>("https://api.qwant.com/api/search/images?offset=0&t=images&uiv=1&q=$query&count=$count") {
        headers.append("user-agent", "Mozilla/5.0")
    }
    return JsonPath.read<List<String>>(json, "$..thumbnail").map { "https:$it" }
}

suspend fun HttpClient.requestImageData(imageUrl: String): BufferedImage {
    val httpResponse = get<HttpResponse>(imageUrl)
    if (httpResponse.status == HttpStatusCode.OK) {
        return ImageIO.read(httpResponse.receive<InputStream>())
    }
    throw IOException("Wrong status code ${httpResponse.status}")
}
//</editor-fold>

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::collage)
    server.start(wait = true)
}

fun Parameters.required(name: String) =
    this[name] ?: throw IllegalStateException("Parameter $name is required")
