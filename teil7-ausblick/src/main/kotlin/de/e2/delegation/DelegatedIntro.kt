package de.e2.delegation

import kotlin.math.PI

class Circle(val x: Double, val y: Double, val radius: Double) {
    val area: Double by lazy {
        println("Berechne Fläche")
        PI * radius * radius
    }
}

fun main() {
    val circle = Circle(0.0, 0.0, 10.0)
    println(circle.area)
    println(circle.area)
}