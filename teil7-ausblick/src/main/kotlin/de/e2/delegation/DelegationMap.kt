package de.e2.delegation

class Person(map: Map<String, Any>) {
    val name: String by map
    val vorname: String by map
}

fun main() {
    val map = mapOf<String, Any>(
        "name" to "Preissel",
        "vorname" to "Rene"
    )
    val person = Person(map)
    println(person.name)
    println(person.vorname)
}