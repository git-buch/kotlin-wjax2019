package de.e2

import io.kotest.matchers.shouldBe
import io.kotest.core.spec.style.StringSpec
import java.util.*
import java.sql.Date as SqlDate
import kotlin.text.toLowerCase as inKleinbuchstaben

class Aliases : StringSpec({

    "alias for multiple Date class in Java stdlibraries" {
        val javaDate = Date()
        val sqlDate = SqlDate(javaDate.time)

        sqlDate.time shouldBe javaDate.time
    }

    "alias for functions" {
        "ABC".inKleinbuchstaben() shouldBe "abc"
    }

})