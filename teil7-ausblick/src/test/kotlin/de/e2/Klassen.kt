package de.e2

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.math.round

class Klassen {
    /**
     * ## Ein `init`-Block wird bei jeder Instanzerzeugung aufgerufen.
     *
     * Wenn man möchte, dass mit dem primären Konstruktor extra Code
     * ausgeführt wird, nimmt man einen `init`-Block.
     */
    @Test
    fun `Wenn vorhanden, wird mit dem primären Konstruktor auch ein init-Block ausgeführt`() {


        class Betrag(b: Double) {

            val betrag: Double

            init {
                betrag = round(b * 100.0) / 100.0
            }
        }


        Assertions.assertEquals(47.11, Betrag(47.1134).betrag)

    }

    // Anmerkung: Es gibt die Möglichkeit, neben dem
    // *primären* noch weitere *sekundäre* Konstruktoren
    // zu definieren. Für diesen Workshop wird das
    // nicht benötigt.
    // Falls Du trotzdem wissen möchtest, wie es geht,
    // findest Du das hier:
    // https://kotlinlang.org/docs/reference/classes.html#constructors

    /**
     * ## Vererbung
     *
     * - Nur erlaubt, wenn Oberklasse als `open` deklariert ist.
     * - Nach dem `:` wird ein Konstruktur der Oberklasse aufgerufen.
     */
    @Test
    fun `Vererbung`() {


        open class Name(val vorname: String, val name: String)

        class Ansprache(val anrede: String, vorname: String, name: String) : Name(vorname, name)


        var a = Ansprache("Lady", "Ada", "Lovelace")
        Assertions.assertEquals("Lady", a.anrede)
        Assertions.assertEquals("Ada", a.vorname)
        Assertions.assertEquals("Lovelace", a.name)
    }


    /**
     * ## Interfaces
     *
     * * Sehr ähnlich wie in Java
     * * `override` muss angegeben werden. (Gilt auch bei Vererbung)
     *
     */
    @Test
    fun `Interfaces`() {

        // Deklaration des Interfaces `HelloSayer` unterhalb dieser Funktion

        class Moin : HelloSayer {

            override fun sayHello() = println("Moin Moin!")

        }

        Moin().sayHello()
    }

    interface HelloSayer {
        fun sayHello()
    }
}