package de.e2

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import io.kotest.core.spec.style.StringSpec

data class PLZ(@JsonValue val plz: Int) {

    companion object {
        @JvmStatic
        @JsonCreator
        fun create(plz: Int) = PLZ(plz)
    }
}

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "species")
@JsonSubTypes(
    Type(value = Katze::class, name = "cat"),
    Type(value = Fisch::class, name = "fish")
)
sealed class Tier

data class Katze(val miau: String) : Tier()
data class Fisch(val flosse: String) : Tier()

data class Zoo(val tiere: List<Tier>)

data class Anschrift(
    val strasse: String,
    val hasnummer: String,
    val plz: PLZ,
    val ort: String
)

class JacksonAndAnnotations : StringSpec({


    "serializing" {
        val mapper = ObjectMapper(JsonFactory())

        mapper
            .writeValueAsString(Anschrift("Abcstr.", "123", PLZ(12345), "Irgendwow"))
            .also { println(it) }

        mapper
            .writeValueAsString(Zoo(listOf(Katze("iiaau"), Fisch("blub"))))
            .also { println(it) }
    }
})