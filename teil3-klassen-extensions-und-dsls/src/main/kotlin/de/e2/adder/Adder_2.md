# HTML-DSL
* [kotlinx.html](https://github.com/Kotlin/kotlinx.html) stellt eine typsichere DSL für HTML bereit
* Der unäre `+`-Operator ist redefiniert worden um Text zu inserten
    * Alternativ geht die Funktion `text ()`
* Eigene Komponenten sind nichts weiter als Extension-Funktionen an vorhandenen DSL-Klassen    


