# Ktor-Module und Methoden-Referenzen

* Ktor-Anwendungen werden normalerweise in Module aufgeteilt
    * Module sind nichts weiter als Extension-Funktionen an der Klasse `Application`
* Anstelle des Lambdas kann eine Referenz auf eine Extension-Funktion beim Definieren des Servers übergeben werden: `Application::helloModule`
* Hat man mehrere Module dann erzeugt man ein Lambda und ruft darin die Modul-Funktionen auf
    * Der Typ des `module`-Parameters ist: `module: Application.() -> Unit`
    * Deswegen hat das Lambda `Application` als `this`-Zeiger
    
# Ktor-Test
* Ktor unterstützt bei Unit-Tests
* Module können im Test ohne Webcontainer gestartet werden: `withTestApplication`
* Test-Requests können aufgerufen werden: `handleRequest` und die Results überprüft werden    
