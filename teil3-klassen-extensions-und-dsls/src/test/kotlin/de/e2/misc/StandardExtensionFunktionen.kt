package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

/**
 * ## Hilfreiche Standardfunktionen
 * * die folgenden Funktionen erlauben idiomatischen Kotlin-Code
 * * sie vermeiden häufig Hilfsvariablen und komplexe Ausdrücke
 *
 * Für die Auswahl siehe:
 * ['Mastering Kotlin standard functions: run, with, let, also and apply'](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)
 */
class StandardExtensionFunktionen {

    /**
     * Listen und Strings haben viele nützliche Extensions
     */
    @Test
    fun `Nuetzliche Extensions`() {

        val numberList = listOf(1, 2)
        val sum = numberList.sum()
        assertEquals(3, sum)

        val first = numberList.first()
        assertEquals(1, first)

        numberList.forEachIndexed { index, i ->
            assertEquals(index, i - 1 )
        }

        assertEquals("Abc", "abc".capitalize())
        assertEquals(null, "abc".toIntOrNull())
    }

    /**
     * `let` erlaubt es den Receiver umzuwandeln, genauso wie `map` bei Collections
     */
    @Test
    fun `let verhält sich für normale Variablen wie map für Collections`() {

        val numberList = listOf(1, 2)
        val squaredList = numberList.map { it * it }
        assertEquals(1, squaredList[0])
        assertEquals(4, squaredList[1])

        //Mit let geht das auch für ein Objekt
        val squared3 = 3.let { it * it }
        assertEquals(9, squared3)

        val squaredFirstOrNull = numberList.firstOrNull()?.let { it * it }
        assertNotNull(squaredFirstOrNull)
    }


    /**
     * `run` ist wie `let`, nur wird der this Zeiger redefiniert
     */
    @Test
    fun `run verhält sich wie let nur mit this-Zeiger`() {
        val squared3 = 3.run { this * this }
        assertEquals(9, squared3)
    }

    /**
     * 'apply' wird benutzt um die Initialisierung zu vereinfachen, wenn kein passender Konstruktor vorhanden ist
     * Ansonsten ist `apply` fast so wie `run`, nur das der Receiver zurückgegeben wird.
     */
    @Test
    fun `apply vereinfacht die Initialisierung`() {
        val numberList = ArrayList<Int>().apply {
            add(1)
            add(2)
        }
        assertEquals(1, numberList[0])
        assertEquals(2, numberList[1])
    }

    /**
     * 'use' ist der Ersatz für `try-with-resources` aus Java
     * * `use` ist eine Extension-Funktion für ein `Closable`
     */
    @Test
    fun `Use schliest Resourcen nach der Benutzung`() {

        File("build/numbers.txt").writer().use { writer ->
            (1..5).forEach {
                writer.write(it)
            }
        }
    }


}


