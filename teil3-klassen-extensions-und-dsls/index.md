# Klassen, Extensions und DSLs

* [Extensions (rp)](src/main/kotlin/de/e2/helloworld/HelloWorld_4a_Eigene_Funktion_Mit_Return.kt) 
             ([A](src/main/kotlin/de/e2/helloworld/HelloWorld_4c_Extension_Funktion.kt))             
* [Standard-Extensions / Standard-Scope-Funktionen](src/test/kotlin/de/e2/misc/StandardExtensionFunktionen.kt)             
* [Ktor Module (rp)](src/main/kotlin/de/e2/helloworld/HelloWorld_5a_Ktor_Modul.kt)    
* [HTML DSL (rp)](src/main/kotlin/de/e2/adder/Adder_2a_Html_DSL.kt) 
            ([B](src/main/kotlin/de/e2/adder/Adder_2b_Html_DSL_mit_Operator.kt),
             [C](src/main/kotlin/de/e2/adder/Adder_2c_Html_DSL_eigene_Komponente.kt))          
* [Uebung 3](../uebungen/Uebung3.md)
