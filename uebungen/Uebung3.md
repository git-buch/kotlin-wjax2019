# Beschreibung der Übung
Erweitere die Anwendung um einen Handler: `http://localhost:8080/spielhtml`
der folgende HTML-Seite erzeugt:

    <html>
      <body>
        <h1>Schnick Schnack Schnuck</h1>
        <form action="/spielergebnis" method="get">
            <label>Deine Wahl:</label>
            <button name="wahl" type="submit" value="papier">Papier</button>
            <button name="wahl" type="submit" value="stein">Stein</button>
        </form>
      </body>
    </html>

Erweitere die Anwendung um einen Handler `http://localhost:8080/spielergebnis?wahl=papier`
der folgende HTML-Seite angepasst an das Spielergebnis erzeugt:
    
    <html>
      <body>
        <h1>Schnick Schnack Schnuck</h1>
        <p>Spieler hat gewählt: PAPIER</p>
        <p>Computer hat gewählt: PAPIER</p>
        <p>Das Ergebnis ist: Unentschieden</p>
        <div>
            <a href="/spielhtml">Nächstes Spiel</a>
        </div>
      </body>
    </html>

## Zusatzaufgaben
* Lagere die HTML-Variante und die Text-Variante in jeweils ein eigenes Modul aus

   
    fun Application.htmlModul()
    fun Application.textModul()

* Erzeuge eine Extension-Funktion um die Formatierung der Paragraphen `<p>...</p>` auszulagern:


    fun BODY.paragraph(s: String)
    
* Konvertiere die Funktion `string2Auswahl()` in eine Extension-Funktion für die Klasse `String`.    


## Manuelle Tests
Gehe auf die Seite: http://localhost:8080/spielhtml und drücke einen Button.
Überprüfe, ob das Ergebnis richtig angezeigt wird.
    

# Kenntnisse
* [HTML-DSL](https://github.com/Kotlin/kotlinx.html#stream)
* HTML zurückgeben: `call.respondHtml{ ... }`
* Extension-Funktionen: `fun String.toInt()`

# Code-Beispiele
* `teil3-klassen-extensions-und-dsls/src/main/kotlin/de/e2/adder`
* `teil3-klassen-extensions-und-dsls/src/test/kotlin/de/e2/misc`