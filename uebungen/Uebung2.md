# Beschreibung der Übung

Erweitere die Anwendung um die Möglichkeit einen Parameter `wahl` mitzugeben: http://localhost:8080/spiel?wahl=papier 
Unterstütze dabei als Auswahl `papier` und `stein`. Implementiere das als eigene Enum-Klasse `Wahl`.
Lass den Computer zufällig aus den beiden Alternativen wählen und ermittle den Sieger oder das Unentschieden.
Erstelle eine eigene Funktion für das Umwandeln des String-Parameters in einen Enum-Wert : `fun string2Wahl(wahl: String): Wahl?` 
Wenn keine oder eine ungültige Wahl getroffen wurde, gib eine entsprechende Fehlermeldung aus. 
Für die Ermittlung des Siegers schreibe eine eigene Funktion: `fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl): String` 

## Manuelle Tests
    http://localhost:8080/spiel?wahl=papier
    -> Spieler hat gewählt: PAPIER
       Computer hat gewählt: STEIN
       Das Ergebnis ist: Spieler hat gewonnen 
 
    http://localhost:8080/spiel
    -> Bitte Parameter 'wahl' angeben
    
    http://localhost:8080/spiel?wahl=apfel
        -> Ungültige Auswahl

## Zusatzaufgabe
* Nutze innerhalb der `ermittleErgebnis()`-Funktion das `when`-Statement 
* Erweitere die Auswahl um die Schere und erweitere die  `ermittleErgebnis()` -Funktion entsprechend.
* Führe einen weiteren Enum für Ergebnis ein: UNENTSCHIEDEN, SPIELER_GEWINNT, COMPUTER_GEWINNT

# Kenntnisse
* `if`-Statement: `if (b) true else false` 
* `call.parameter["name]` für Zugriff auf Parameter
* Nullable-Typen mit `?` am Ende. Vor dem Zugriff auf `null` prüfen
    * `?.` Safe Access Operator
    * `?:` Elvis-Operator 
* Funktionsdefinition `fun add(a: Int, b: Int): Int`
    * Parametertypen nach dem Namen
    * Return-Type nach den Parametern
* Vergleiche mit `==` anstelle von `equals`
* `listOf(a,b)` zum Anlegen von Listen 
    * `random()` als Funktion an Liste um zufälliges Element zu erhalten
* Template-Strings: `"Hallo $name`
* Enums: `enum class Farbe`
* `when`-Konstrukt: `when(i) { 1 -> "eins" }`, `when { i == 1 -> "eins"; else -> "viele" }`

# Code-Beispiele

* `teil2-funktionen-und-datentypen/src/main/kotlin/de/e2/helloworld`
* `teil2-funktionen-und-datentypen/src/test/kotlin/de/e2/misc`