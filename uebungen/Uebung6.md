# Beschreibung der Übung

1. Erstelle einen Unit-Test für die Funktion `ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl)` aus der 
vorherigen Übung 3. Nutze dafür die "data driven" Variante (`row`, `forAll`).
2. Erstelle einen Integrationstest für den Rest-Service der letzten Übung. überprüfe nur, das ein Ergebnis vorliegt, nicht welches.
(Da der Computer mittels `random` seine Wahl trifft.) 

    
## Zusatzaufgabe
Refaktoriere den Code der Übung 5 so, dass das `Random`-Objekt durch einen Mock ausgetauscht werden kann.
Schreibe einen Unit-Test, der mit Mockk das `Random`-Objekt ersetzt. Teste die Aufrufe des Mocks.
   

# Kenntnisse
* KoTest
* Mockk  

# Code-Beispiele
* `teil6-testen/src/test/kotlin/de/e2/testen/KoTest.kt`
* `teil6-testen/src/test/kotlin/de/e2/testen/AdderKtorTest.kt`
* `teil6-testen/src/test/kotlin/de/e2/testen/Mocking.kt`
