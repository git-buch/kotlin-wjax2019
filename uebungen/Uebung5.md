# Beschreibung der Übung

Erzeuge ein neues Modul für eine Rest-basierte API, um gegen den Computer zu spielen: `http://localhost:8080/api`

    
    POST http://localhost:8080/api
    Content-Type: application/json
    Accept: application/json
    
    {
      "wahl": "PAPIER"
    }

Erzeuge zwei Data-Klassen: `ApiRequest(wahl)`und `ApiResponse(spielerWahl, computerWahl, ergebnis)` um die Auswahl 
entgegen zu nehmen und das Ergebnis zu liefern.
Lass den Computer zufällig eine Antwort auswählen und gib das passende Ergebnis zurück.

    {
      "spielerWahl": "PAPIER",
      "computerWahl": "PAPIER",
      "ergebnis": "UNENTSCHIEDEN"
    }


Schreibe einen passenden Client der die Rest-Api aufruft und das Ergebnis ausgibt.

## Manuelle Tests

* Starte den Server und rufe den Client auf.
    
## Zusatzaufgaben

* Nutze im Client Destructuring um an die einzelnen Ergebnisteile zu kommen 
    

# Kenntnisse
* Installation des Jackson-Features: `install(ContentNegotiation)`
* Data-Klassen: `data class Person(val name: String)`
* Http-Client als Koroutinen aufrufen: `runBlocking()`
* Http-Client benutzen
  * `post<ApiResponse> { ... }`    

# Code-Beispiele
* `teil5-rest-services-und-data-classes/src/main/kotlin/de/e2/adder/Adder_Rest_Api.kt`
* `teil5-rest-services-und-data-classes/src/main/kotlin/de/e2/adder/Adder_Rest_Client.kt`