package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.math.round

/**
 * ## Klassen in Kotlin
 *
 * - Besonderheiten bei der Deklaration
 * - Objekte (Instanzen) erzeugen.
 * - Primäre Konstruktoren
 * - Vererbung
 *
 */
class Klassen {


    /**
     * ### Deklaration und Konstrukturaufruf
     *
     *
     *  * Deklarationsblock `{...}` ist optional.
     *
     *  * Geschachtelte Deklaration, z. B. innerhalb von Funktionen,
     *    ist erlaubt.
     *
     *  * Kein `new` beim Konstruktoraufruf
     *    - Konstruktoren erkennt man daran,
     *      dass sie mit einem Großbuchstaben beginnen.
     *
     */
    @Test
    fun `Die kleinste aller Klassen`() {

        class FastNix

        val n = FastNix()

        assertTrue(n.toString().contains("FastNix"))
        assertTrue(n is FastNix)
    }


    /**
     * ## Felder und Funktionen
     *
     *  * *Felder* als `val` oder `var`
     *
     *  * *Funktionen* (aka *Methoden*) als `fun`
     *
     *  * `public` ist Default und wird meist weggelassen.
     *
     */
    @Test
    fun `Eine einfache Klasse`() {


        class Rechteck {

            var breite = 0
            var hoehe = 0

            fun berechneFlaeche() = breite * hoehe

        }


        val r = Rechteck()
        r.breite = 3
        r.hoehe = 4


        val flaeche = r.berechneFlaeche()

        assertEquals(3, r.breite)
        assertEquals(4, r.hoehe)
        assertEquals(12, flaeche)
    }


    /**
     * ## Primärer Konstruktor
     *
     * * Jede Klasse hat genau einen(!) *primären Konstruktor*
     *
     * * Dieser wird bei *jeder* Instanzerzeugung aufgerufen.
     *
     * * Die Parameter dürfen im Deklarationsblock zur Initialisierung von
     *   Feldern genutzt werden.
     */
    @Test
    fun `Ein primärer Konstruktor`() {

        class Rechteck(b: Int, h: Int) {

            val breite = b
            val hoehe = h

        }

        val r = Rechteck(3, 4)

        assertEquals(3, r.breite)
        assertEquals(4, r.hoehe)
    }

    /**
     * ## Felddeklarationen im primären Konstruktor
     *
     * Viele Konstruktoren tun nichts weiter,
     * als ein paar Felder zu initialiseren.
     * Dafür gibt es eine Kurznotation.
     * Wenn man den Parametern des primären Konstruktors `val` oder
     * `var` vorangestellt, wird ein ex
     */
    @Test
    fun `Mit var oder val könen Felder auch im primären Konstruktur deklariert werden`() {

        class Rechteck(val breite: Int, val hoehe: Int)

        val r = Rechteck(3, 4)

        assertEquals(3, r.breite)
        assertEquals(4, r.hoehe)
    }



}