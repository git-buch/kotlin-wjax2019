val ktorVersion: String by rootProject.extra
val junitVersion: String by rootProject
val log4jVersion: String by rootProject


plugins {
    kotlin("jvm")
}

dependencies {
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-html-builder:$ktorVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")

    runtimeOnly("org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion")
}

